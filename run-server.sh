export PYTHONPATH=.
uvicorn app:app --reload --host ${SERVER_HOST} --port ${SERVER_PORT}
