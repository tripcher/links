import abc
import typing

from . import models


class DomainsRepository(abc.ABC):
    """Коллекция доменов"""

    def add(self, *items: 'models.Domain') -> None:
        """
        Добавляет елементы в коллекцию.
        Args:
            *items (models.Domain):

        Returns:
            None:

        Raises:
            FailedAddObject:
        """
        pass

    def get(self, of: typing.Optional[int] = None, to: typing.Optional[int] = None) -> typing.Iterable['models.Domain']:
        """
        Возвращает елементы из коллекции.
        Args:
            of (typing.Optional[int]): временная метка в секундах
            to (typing.Optional[int]): временная метка в секундах

        Returns:
            typing.Iterable['models.Domain']:

        Raises:
            FailedGetObject:
        """
        pass
