import typing

import pydantic
from pydantic import networks

from . import models, types


class LinksBase(pydantic.BaseModel):
    links: typing.List[types.LINK_TYPE]

    @pydantic.validator('links', each_item=True)
    def name_must_contain_space(cls, v):
        if not isinstance(v, pydantic.HttpUrl):
            if len(v) > models.MAX_LENGTH_DOMAIN:
                raise pydantic.AnyStrMaxLengthError(limit_value=models.MAX_LENGTH_DOMAIN)

            is_international = False
            d = networks.ascii_domain_regex().fullmatch(v)
            if d is None:
                d = networks.int_domain_regex().fullmatch(v)
                if d is None:
                    raise pydantic.errors.UrlHostError()
                is_international = True

            tld = d.group('tld')

            if tld is None and not is_international:
                d = networks.int_domain_regex().fullmatch(v)
                tld = d.group('tld')

            if tld is None:
                raise pydantic.errors.UrlHostTldError()
        return v


class GetParameters(pydantic.BaseModel):
    of: typing.Optional[int]
    to: typing.Optional[int]
