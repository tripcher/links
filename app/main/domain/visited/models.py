import pydantic

from . import types

MAX_LENGTH_DOMAIN = 2 ** 16


class Domain(pydantic.BaseModel):
    """Модель домена."""

    value: str = pydantic.Field(..., max_length=MAX_LENGTH_DOMAIN)

    @classmethod
    def from_link(cls, link: types.LINK_TYPE) -> 'Domain':
        return cls(value=link.host) if isinstance(link, pydantic.HttpUrl) else cls(value=link)
