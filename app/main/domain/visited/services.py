import typing

from . import models, repositories, schemas


def add_visited_domains(data: schemas.LinksBase, domains_rep: repositories.DomainsRepository) -> None:
    """
    Добавляет домены в коллекцию.
    Args:
        data (schemas.LinksBase):
        domains_rep (repositories.DomainsRepository):

    Returns:
        None:

    Raises:
        FailedAddObject:
    """
    domains: typing.Iterable['models.Domain'] = map(lambda item: models.Domain.from_link(item), data.links)
    domains_rep.add(*domains)


def get_visited_domains(
        params: schemas.GetParameters,
        domains_rep: repositories.DomainsRepository
) -> typing.Iterable['models.Domain']:
    """
    Возвращает домены из коллекции.
    Args:
        params (schemas.GetParameters):
        domains_rep (repositories.DomainsRepository):

    Returns:
        typing.Iterable['models.Domain']:

    Raises:
        FailedGetObject:
    """
    return domains_rep.get(of=params.of, to=params.to)
