import typing

import pydantic

LINK_TYPE = typing.Union[pydantic.HttpUrl, str]
