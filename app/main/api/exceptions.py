import enum

from fastapi import exceptions, status


class KeysError(str, enum.Enum):
    """Ключи ошибок."""

    SERVER_ERROR = 'SERVER_ERROR'
    VALIDATION_ERROR = 'VALIDATION_ERROR'


class ServerErrorHTTPException(exceptions.HTTPException):
    def __init__(self) -> None:
        super().__init__(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, detail=KeysError.SERVER_ERROR)
