from . import controllers

__all__ = ('router',)

router = controllers.router
