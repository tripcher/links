import typing

from fastapi import APIRouter, Depends

from app.core import db as database
from app.core import exceptions
from app.main.api import exceptions as api_exceptions
from app.main.domain.visited import models
from app.main.domain.visited import repositories as domain_rep
from app.main.domain.visited import schemas, services
from app.main.implement.visited import repositories as impl_rep

router = APIRouter()

response_ok = {
    'status': 'ok'
}


def get_domain_repository(db=Depends(database.get_db)):
    return impl_rep.DomainsRedisRepository(db)


@router.post('/links')
def add_visited_domains(
        data: schemas.LinksBase,
        domains_rep: domain_rep.DomainsRepository = Depends(get_domain_repository)
):
    try:
        services.add_visited_domains(data, domains_rep)
    except exceptions.FailedAddObject:
        raise api_exceptions.ServerErrorHTTPException()

    return response_ok


@router.get('/domains')
def get_visited_domains(
        of: typing.Optional[int] = None,
        to: typing.Optional[int] = None,
        domains_rep: domain_rep.DomainsRepository = Depends(get_domain_repository)
):
    params = schemas.GetParameters(of=of, to=to)

    try:
        domains: typing.Iterable['models.Domain'] = services.get_visited_domains(params, domains_rep)
    except exceptions.FailedGetObject:
        raise api_exceptions.ServerErrorHTTPException()

    return {
        'domains': [item.value for item in domains],
        **response_ok
    }
