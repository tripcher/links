from fastapi import APIRouter

from .visited import router as visited_router

__all__ = ('router_v1',)

router_v1 = APIRouter()


router_v1.include_router(
    visited_router,
    prefix="/visited",
    tags=["visited"]
)
