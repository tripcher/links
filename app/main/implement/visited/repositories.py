import typing
from datetime import datetime

import redis

from app.core import exceptions
from app.main.domain.visited import models, repositories


class DomainsRedisRepository(repositories.DomainsRepository):
    """Коллекция доменов в Redis"""

    COLLECTION_NAME = 'domains'

    def __init__(self, db: redis.Redis):
        self.db = db

    def add(self, *items: 'models.Domain') -> None:
        mapping: typing.Dict[str, int] = {item.value: int(datetime.utcnow().timestamp()) for item in items}
        try:
            self.db.zadd(self.COLLECTION_NAME, mapping=mapping)
        except redis.exceptions.RedisError:
            raise exceptions.FailedAddObject()

    def get(self, of: typing.Optional[int] = None, to: typing.Optional[int] = None) -> typing.Iterable['models.Domain']:
        try:
            zrange: typing.List[str] = \
                self.db.zrangebyscore(self.COLLECTION_NAME, min=of or '-inf', max=to or '+inf')
        except redis.exceptions.RedisError:
            raise exceptions.FailedGetObject()
        return map(lambda item: models.Domain(value=item), zrange)
