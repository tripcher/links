import typing

from pydantic import BaseSettings


class Settings(BaseSettings):

    REDIS_HOST: str = 'localhost'
    REDIS_PORT: int = 6379
    REDIS_DB: int = 0
    REDIS_PASSWORD: typing.Optional[str] = None

    TEST_REDIS_HOST: str = 'localhost'
    TEST_REDIS_PORT: int = 6379
    TEST_REDIS_DB: int = 1
    TEST_REDIS_PASSWORD: typing.Optional[str] = None

    class Config:
        case_sensitive = True
        env_file = '.env'

