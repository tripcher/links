class FailedAddObject(Exception):
    """Не удалось добавить объект в коллекцию."""
    pass


class FailedGetObject(Exception):
    """Не удалось вернуть объект из коллекции."""
    pass
