import os

from fastapi import FastAPI, HTTPException, Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse

from app.main import api
from app.main.api.exceptions import KeysError

APP_DIR = os.path.dirname(os.path.realpath(__file__))

BASE_DIR = os.path.dirname(APP_DIR)

app = FastAPI()

app.include_router(
    api.router_v1,
    prefix='/api/v1'
)


@app.exception_handler(HTTPException)
def http_exception_handler(request: Request, exc: HTTPException):
    return JSONResponse(
        status_code=exc.status_code,
        content={
            'status': exc.detail
        },
    )


@app.exception_handler(RequestValidationError)
def validation_exception_handler(request: Request, exc: RequestValidationError):
    return JSONResponse(
        status_code=422,
        content={
            'status': KeysError.VALIDATION_ERROR,
            'errors': exc.errors()
        },
    )


@app.get('/')
def root():
    return {'message': 'Links service'}
