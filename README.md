# Test Links

## Installing
```
git clone git@bitbucket.org:tripcher/links.git
cd ./links
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

## Installing Redis
https://redis.io/topics/quickstart

## Run
```
uvicorn app:app --reload
```

## Run dev
```
export PYTHONPATH=.
python ./app/run.py
```

## Run docker
```
docker-compose up --build
```

## Testing
```
pytest --flake8 --isort --mypy --cov=app tests/
```
