from fastapi.testclient import TestClient

from app import app

client = TestClient(app)


def test_add_get_visited_links__usual(override_db):
    links = [
        'https://google.com',
        'http://ya.ru?q=123',
        'funbox.ru',
        'https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor'
    ]

    domains = [
        'ya.ru',
        'funbox.ru',
        'stackoverflow.com',
        'google.com'
    ]

    data = {
        'links': links
    }

    client.post('/api/v1/visited/links', json=data)

    response = client.get('/api/v1/visited/domains')
    response_body = response.json()

    assert len(response_body['domains']) == len(domains)
    assert all([item in domains for item in response_body['domains']])


def test_add_get_visited_links__duplication(override_db):
    # TODO: пока считаем, что www.ya.ru и ya.ru - разные домены, нужно добавить удаление www в схему или в from_link
    links = [
        'https://ya.ru',
        'https://ya.ru?q=123',
        'http://ya.ru',
        # 'https://www.ya.ru',
        'ya.ru'
    ]

    domains = [
        'ya.ru',
    ]

    data = {
        'links': links
    }

    client.post('/api/v1/visited/links', json=data)

    response = client.get('/api/v1/visited/domains')
    response_body = response.json()

    assert len(response_body['domains']) == len(domains)
    assert all([item in domains for item in response_body['domains']])
