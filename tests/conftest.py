import typing

import pytest
import redis

from app import app
from app.core import db as database
from app.core.conf import settings
from app.main.domain.visited import models
from app.main.implement.visited import repositories as impl_rep

test_db_redis = redis.Redis(
    host=settings.TEST_REDIS_HOST,
    port=settings.TEST_REDIS_PORT,
    db=settings.TEST_REDIS_DB,
    password=settings.TEST_REDIS_PASSWORD
)


@pytest.fixture()
def db() -> typing.Generator[redis.Redis, None, None]:
    """
    Возвращает тестовый экземпляр Redis и отчищает бд по окончанию теста.
    Returns:
        typing.Generator[redis.Redis, None, None]:
    """
    db_ = test_db_redis

    yield db_

    db_.flushdb()


@pytest.fixture()
def override_db(db) -> typing.Generator[None, None, None]:
    """
    Возвращает тестовый экземпляр Redis и отчищает бд по окончанию теста.
    Returns:
        typing.Generator[None, None, None]:
    """

    def override_get_db():
        return db

    app.dependency_overrides[database.get_db] = override_get_db

    yield

    db.flushdb()


@pytest.fixture()
def domains_db(
        domains: typing.List['models.Domain'],
        db: redis.Redis
) -> typing.List[typing.Tuple['models.Domain', int]]:
    """
    Создает в БД (Redis) и возвращает список доменов.

    Добавляет данные в бд со скором.

    Args:
        domains (typing.List['models.Domain']):
        db (redis.Redis):

    Returns:
        typing.List[typing.Tuple['models.Domain', int]]:

    """
    domains_score = list(zip(domains, range(len(domains))))
    mapping: typing.Dict[str, int] = {domain.value: score for domain, score in domains_score}
    db.zadd(impl_rep.DomainsRedisRepository.COLLECTION_NAME, mapping=mapping)
    return domains_score


@pytest.fixture()
def domains() -> typing.List['models.Domain']:
    """
    Возвращает список доменов.
    Returns:
        typing.List['models.Domain']:
    """
    return [
        models.Domain(value='ya.ru'),
        models.Domain(value='funbox.ru'),
        models.Domain(value='stackoverflow.com')
    ]


@pytest.fixture()
def domains_duplication() -> typing.List['models.Domain']:
    """
    Возвращает список доменов c дублями.
    Returns:
        typing.List['models.Domain']:
    """
    return [
        models.Domain(value='ya.ru'),
        models.Domain(value='ya.ru'),
        models.Domain(value='ya.ru'),
        models.Domain(value='funbox.ru'),
        models.Domain(value='funbox.ru'),
    ]
