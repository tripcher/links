import typing

import redis

from app.main.domain.visited import models
from app.main.implement.visited import repositories as impl_rep

# TODO: добавить тесты для слоя services


def test_redis__add__usual(domains: typing.List['models.Domain'], db: redis.Redis) -> None:
    """
    Добавляет 3 уникальных домена в redis из уникального списка.
    Args:
        domains (typing.List['models.Domain']):
        db (redis.Redis):

    Returns:
        None:
    """
    assert len(domains) == 3

    rep = impl_rep.DomainsRedisRepository(db)

    rep.add(*domains)

    result = db.zrangebyscore(rep.COLLECTION_NAME, min='-inf', max='+inf')

    assert len(result) == 3
    assert all([models.Domain(value=item) in domains for item in result])


def test_redis__add__duplication(domains_duplication: typing.List['models.Domain'], db: redis.Redis):
    """
    Добавляет 2 уникальных домена в redis из списка с дублями.
    Args:
        domains_duplication (typing.List['models.Domain']): лист доменов с дублями
        db (redis.Redis):

    Returns:
        None:
    """
    assert len(domains_duplication) > 2

    rep = impl_rep.DomainsRedisRepository(db)

    rep.add(*domains_duplication)

    result = db.zrangebyscore(rep.COLLECTION_NAME, min='-inf', max='+inf')

    assert len(result) == 2
    assert all([models.Domain(value=item) in domains_duplication for item in result])


def test_redis__get__all(domains_db: typing.List[typing.Tuple['models.Domain', int]], db: redis.Redis):
    """
    Возвращает все домены

    of = None
    to = None

    Args:
        domains_db (typing.List[typing.Tuple['models.Domain', int]]):
        db (redis.Redis):

    Returns:
        None:
    """
    rep = impl_rep.DomainsRedisRepository(db)

    result = list(rep.get())

    exp_result = list(map(lambda item: item[0], domains_db))

    assert len(result) == len(exp_result)
    assert all([item in exp_result for item in result])


def test_redis__get__of(domains_db: typing.List[typing.Tuple['models.Domain', int]], db: redis.Redis):
    """
    Возвращает домены score которых >= 1

    of = 1
    to = None

    Args:
        domains_db (typing.List[typing.Tuple['models.Domain', int]]):
        db (redis.Redis):

    Returns:
        None:
    """
    rep = impl_rep.DomainsRedisRepository(db)

    result = list(rep.get(of=1))

    exp_result = list(map(lambda item: item[0], filter(lambda item: item[1] >= 1, domains_db)))

    assert len(result) == len(exp_result)
    assert all([item in exp_result for item in result])


def test_redis__get__to(domains_db: typing.List[typing.Tuple['models.Domain', int]], db: redis.Redis):
    """
    Возвращает домены score которых <= 1

    of = None
    to = 1

    Args:
        domains_db (typing.List[typing.Tuple['models.Domain', int]]):
        db (redis.Redis):

    Returns:
        None:
    """
    rep = impl_rep.DomainsRedisRepository(db)

    result = list(rep.get(to=1))

    exp_result = list(map(lambda item: item[0], filter(lambda item: item[1] <= 1, domains_db)))

    assert len(result) == len(exp_result)
    assert all([item in exp_result for item in result])


def test_redis__get__of__to(domains_db: typing.List[typing.Tuple['models.Domain', int]], db: redis.Redis):
    """
    Возвращает домены score которых <= 1 и >= 1

    of = 1
    to = 1

    Args:
        domains_db (typing.List[typing.Tuple['models.Domain', int]]):
        db (redis.Redis):

    Returns:
        None:
    """
    rep = impl_rep.DomainsRedisRepository(db)

    result = list(rep.get(of=1, to=1))

    exp_result = list(map(lambda item: item[0], filter(lambda item: 1 <= item[1] <= 1, domains_db)))

    assert len(result) == len(exp_result)
    assert all([item in exp_result for item in result])
