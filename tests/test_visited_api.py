from unittest import mock

from fastapi.testclient import TestClient

from app import app
from app.core import exceptions
from app.main.api import exceptions as api_exceptions
from app.main.domain.visited import models

client = TestClient(app)


@mock.patch('app.main.domain.visited.services.add_visited_domains', return_value=None)
def test_get_visited_links__usual(mock_service):
    data = {
        'links': [
            'https://www.google.com',
            'http://ya.ru?q=123',
            'funbox.ru',
            'https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor'
        ]
    }
    response = client.post('/api/v1/visited/links', json=data)
    assert response.status_code == 200
    assert response.json() == {'status': 'ok'}


@mock.patch('app.main.domain.visited.services.add_visited_domains', return_value=None)
def test_get_visited_links__duplication(mock_service):
    data = {
        'links': [
            'https://ya.ru',
            'https://ya.ru?q=123',
            'http://ya.ru',
            'https://www.ya.ru',
        ]
    }
    response = client.post('/api/v1/visited/links', json=data)
    assert response.status_code == 200
    assert response.json() == {'status': 'ok'}


@mock.patch('app.main.domain.visited.services.add_visited_domains', return_value=None)
def test_get_visited_links__invalid_url(mock_service):
    data = {
        'links': [
            'funbox',
        ]
    }
    response = client.post('/api/v1/visited/links', json=data)
    response_body = response.json()
    assert response.status_code == 422
    assert api_exceptions.KeysError.VALIDATION_ERROR == response_body['status']
    assert 'errors' in response_body.keys()


@mock.patch('app.main.domain.visited.services.add_visited_domains', return_value=None)
def test_get_visited_links__invalid_schema(mock_service):
    data = {
        'links': [
            # FIXME: 'htt://ya.ru'  - возникает баг при сериализации ошибки из за exc.errors()
            'h://ya.ru'
        ]
    }
    response = client.post('/api/v1/visited/links', json=data)
    response_body = response.json()
    assert response.status_code == 422
    assert api_exceptions.KeysError.VALIDATION_ERROR == response_body['status']
    assert 'errors' in response_body.keys()


@mock.patch('app.main.domain.visited.services.add_visited_domains', return_value=None)
def test_get_visited_links__invalid_format(mock_service):
    data = {
        'links': [
            'http:/ya.ru',
        ]
    }
    response = client.post('/api/v1/visited/links', json=data)
    response_body = response.json()
    assert response.status_code == 422
    assert api_exceptions.KeysError.VALIDATION_ERROR == response_body['status']
    assert 'errors' in response_body.keys()


@mock.patch('app.main.domain.visited.services.add_visited_domains', return_value=None)
def test_get_visited_links__invalid_length_url(mock_service):
    data = {
        'links': [
            'https://ya.ru?q=123' + 'x' * models.MAX_LENGTH_DOMAIN,
        ]
    }
    response = client.post('/api/v1/visited/links', json=data)
    response_body = response.json()
    assert response.status_code == 422
    assert api_exceptions.KeysError.VALIDATION_ERROR == response_body['status']
    assert 'errors' in response_body.keys()


@mock.patch('app.main.domain.visited.services.add_visited_domains', return_value=None)
def test_get_visited_links__invalid_length_domain(mock_service):
    data = {
        'links': [
            'x' * models.MAX_LENGTH_DOMAIN + '.com',
        ]
    }
    response = client.post('/api/v1/visited/links', json=data)
    response_body = response.json()
    assert response.status_code == 422
    assert api_exceptions.KeysError.VALIDATION_ERROR == response_body['status']
    assert 'errors' in response_body.keys()


@mock.patch('app.main.domain.visited.services.add_visited_domains', side_effect=exceptions.FailedAddObject())
def test_get_visited_links__fail_add(mock_service):
    data = {
        'links': [
            'https://ya.ru',
        ]
    }
    response = client.post('/api/v1/visited/links', json=data)
    assert response.status_code == 503
    assert response.json() == {'status': api_exceptions.KeysError.SERVER_ERROR}


def test_get_visited_domains__all(domains):
    with mock.patch('app.main.domain.visited.services.get_visited_domains', return_value=domains):
        response = client.get('/api/v1/visited/domains')
    assert response.status_code == 200
    assert response.json() == {
        'domains': [item.value for item in domains],
        'status': 'ok'
    }


def test_get_visited_domains__of(domains):
    with mock.patch('app.main.domain.visited.services.get_visited_domains', return_value=domains):
        response = client.get('/api/v1/visited/domains', params={'of': 100})
    assert response.status_code == 200
    assert response.json() == {
        'domains': [item.value for item in domains],
        'status': 'ok'
    }


def test_get_visited_domains__to(domains):
    with mock.patch('app.main.domain.visited.services.get_visited_domains', return_value=domains):
        response = client.get('/api/v1/visited/domains', params={'to': 100})
    assert response.status_code == 200
    assert response.json() == {
        'domains': [item.value for item in domains],
        'status': 'ok'
    }


def test_get_visited_domains__of_to(domains):
    with mock.patch('app.main.domain.visited.services.get_visited_domains', return_value=domains):
        response = client.get('/api/v1/visited/domains', params={'of': 10, 'to': 100})
    assert response.status_code == 200
    assert response.json() == {
        'domains': [item.value for item in domains],
        'status': 'ok'
    }


def test_get_visited_domains__fail_get(domains):
    with mock.patch(
            'app.main.domain.visited.services.get_visited_domains',
            side_effect=exceptions.FailedGetObject()
    ):
        response = client.get('/api/v1/visited/domains')
    assert response.status_code == 503
    assert response.json() == {'status': api_exceptions.KeysError.SERVER_ERROR}
