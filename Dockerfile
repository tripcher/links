FROM python:3.7

RUN useradd --create-home --user-group user
RUN mkdir /home/user/project
WORKDIR /home/user/project

COPY --chown=user:user ./requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY --chown=user:user . .
RUN chmod u=rwx,g=rx,o=rx wait-for-it.sh

ENTRYPOINT /bin/sh run-server.sh
